<?php

class MediaDaylifeBrowser extends MediaBrowserPlugin {
  function view() {
    $search_form = drupal_get_form('daylife_media_search_form', $this->params);
    $search_form = drupal_render($search_form);
    $media_path = drupal_get_path('module', 'media');
    $dlpath = drupal_get_path('module', 'daylife_media');

    return array(
      '#settings' => array(
        'viewMode' => 'thumbnails',
        'getMediaUrl' => url('daylife/media/list'),
      ) + $this->params,
      '#attached' => array(
        'js' => array(
          $media_path . '/js/plugins/media.library.js',
          $dlpath . '/daylife_media.library.js',
        ),
        'css' => array(
          //@todo: should move this.
          $media_path . '/js/plugins/media.library.css',
          $dlpath . '/daylife_media.library.css',
        ),
      ),
      '#markup' => '<div id="container"><div id="scrollbox">' . $search_form . '<ul id="media-browser-library-list" class="media-list-thumbnails"></ul><div id="status"></div></div></div>',
      );
  }
}
