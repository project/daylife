<?php

class MediaDaylifeStreamWrapper extends MediaReadOnlyStreamWrapper {
  public function getTarget() {
    return FALSE;
  }

  public static function getMimeType($uri, $mapping = NULL) {
    return 'image/daylife';
  }

  public function interpolateUrl() {
    if (isset($this->parameters['p'])) {
      return $this->base_url . check_plain($this->parameters['u']) . '/100x100.jpg';
    }
    return NULL;
  }

  public function getImageID() {
    if ($url = parse_url($this->uri)) {
      if ($url['scheme'] == 'daylife' && is_numeric($url['host'])) {
        return $url['host'];
      }
    }

    return NULL;
  }

  public function setUri($uri) {
    $this->uri = $uri;
  }
}
