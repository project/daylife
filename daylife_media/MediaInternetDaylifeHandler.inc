<?php

class MediaInternetDaylifeHandler extends MediaInternetBaseHandler {
  public function parse($embedCode) {

  }

  public function validate() {

  }

  public function save() {
    $file = $this->getFileObject();
    file_save($file);
    return $file;
  }

  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    return daylife_media_file_uri_to_object($uri);
  }

  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }
}
